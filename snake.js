(function(){
	const Key = {
		left_arrow: 37,
		up_arrow: 38,
		right_arrow: 39,
		down_arrow: 40,
	};

	const Grid = {
		columns: 25,
		rows: 20,
		isFoodSet: false,
		foodLocation: undefined,
	};

	const Game = {
		score: 0,
		maxFps: 10,
		offSet: 1,
		resetGame: false
	};

	const Cell =  {
		empty: 0,
		snake: 1,
		food: 2,
		size: 20,
	};

	const Color = {
		empty: 'white',
		snake: 'blue',
		food: 'red',
	};

	class Position {
		constructor(x, y) {
			this.x = x;
			this.y = y;
		}
	};

	class Snake {
		constructor(initialX, initialY, initialDirection) {
			this.direction = initialDirection;
			this.positions = [];
			this.positions.push(new Position(initialX, initialY));
		}
		get head() {
			return this.positions.slice(-1)[0];
		}

		get tail() {
			return this.positions[0]
		}
		//Is there Food on the next Cell that we intend to move to?
		foodOnNextCell(x, y) {
			if(Grid.foodLocation !== undefined && Grid.foodLocation.x === x && Grid.foodLocation.y === y)
				return true;
			return false;
		}
		eraseTail() {
			let tailPosition = this.positions.shift();
			clearCell(tailPosition.x, tailPosition.y);
		}
		addNewHead(x, y, direction) {
			let newHead = new Position(x, y);
			this.positions.push(newHead);
			this.direction = direction;
		}

		reset() {
			window.initialize();
		}
		increaseScoreAndResetFood(){
			Grid.isFoodSet = false;
			Game.score += 20;

		}
		//Moves one step in the specified direction
		//if not specified then just move in the default(up) direction
		//It is guaranteed that Food is set before invoking this method
		move(direction) {
			let oldRow = this.head.x;
			let oldColumn = this.head.y;
			if(direction === 'left' && oldColumn - 1 >= 0 && !this.snakeCollidesWithItself(oldRow, oldColumn - 1)) {
				if(!this.foodOnNextCell(oldRow, oldColumn - 1))
					this.eraseTail();
				else this.increaseScoreAndResetFood();
				this.addNewHead(oldRow, oldColumn - 1, direction);
			}
			else if(direction === 'right' && oldColumn + 1 < Grid.columns && !this.snakeCollidesWithItself(oldRow, oldColumn + 1)) {
				if(!this.foodOnNextCell(oldRow, oldColumn + 1))
					this.eraseTail();
				else this.increaseScoreAndResetFood();

				this.addNewHead(oldRow, oldColumn + 1, direction);

			}
			else if(direction === 'up' && oldRow - 1 >= 0 && !this.snakeCollidesWithItself(oldRow - 1, oldColumn)) {
				if(!this.foodOnNextCell(oldRow - 1, oldColumn))
					this.eraseTail();
				else this.increaseScoreAndResetFood();
				this.addNewHead(oldRow - 1, oldColumn, direction);
			}
			else if(direction === 'down' && oldRow + 1 < Grid.rows && !this.snakeCollidesWithItself(oldRow + 1, oldColumn)) {
				if(!this.foodOnNextCell(oldRow + 1, oldColumn))
					this.eraseTail();
				else this.increaseScoreAndResetFood();
				this.addNewHead(oldRow + 1, oldColumn, direction);
			}
			else {
				Game.resetGame = true;
			}
		}
		//Will the snake collide with itself with the next move?
		snakeCollidesWithItself(x, y){
			let length = this.positions.length;
			for(let i = 0; i < length; i++) {
				if(this.positions[i].x === x && this.positions[i].y === y) {
					return true;
				}
			}
			return false;
		}
		draw() {
			let length = this.positions.length;
			console.log(length);
			for(let i = 0; i < length; i++) {
				changeCellColor(this.positions[i].x, this.positions[i].y, Color.snake)
			}
		}
	}
	//Find an unoccupied random Cell and place Food there
	function setFood() {
		if(Grid.isFoodSet) return;
		var randomCell = null;
		while(true) {
			randomCell = getRandomCell();
			let length = snake.positions.length;
			let passed = 0;
			for(let i = 0; i < length; i++)
				if(randomCell.x !== snake.positions[i].x && randomCell.y !== snake.positions[i].y)
					passed++;
			if(passed === length)
				break;
		}
		changeCellColor(randomCell.x, randomCell.y, Color.food);
		Grid.isFoodSet = true;
		Grid.foodLocation = {x: randomCell.x, y: randomCell.y};
	}

	function getRandomCell() {
		var x = Math.floor(Math.random() * (Grid.rows));
		var y = Math.floor(Math.random() * (Grid.columns));
		return {x : x, y : y};
	}


	function main() {

		context = getCanvasContext();
		context.font = "12px Monospace";
		key_pressed = {};

		document.addEventListener("keydown", function(event) {
			key_pressed[event.keyCode] = true;
		});

		document.addEventListener("keyup", function(event) {
			delete key_pressed[event.keyCode];
		});

		initialize();
	}

	function drawScore() {
		context.clearRect(canvas.width - 30, canvas.height - 30, 30, 30);
		context.fillText(Game.score.toString(), canvas.width - 30,  canvas.height - 10);
	}

	function resetGame() {
		context.clearRect(0, 0, canvas.width, canvas.height);
		Grid.isFoodSet = false;
		Game.resetGame = false;
		Game.score = 0;
	}

	function initialize() {
		resetGame();
		snake = new Snake(Math.floor(Grid.rows/2), Math.floor(Grid.columns/2), 'up');
		snake.draw();
		loop();
	}


	function loop() {
		setTimeout(function() {
			update();
			draw();
			if(!Game.resetGame) {
				requestAnimationFrame(loop);
			}
			else{
				//alert("Don't be so hard on yourself " + Game.score.toString() + " is not a bad score...");
				initialize();
			}
		}, 1000/Game.maxFps);
	}


	function update() {
		if(key_pressed[Key.left_arrow] && snake.direction !== 'right')
			snake.move('left');
		else if(key_pressed[Key.right_arrow] && snake.direction !== 'left')
			snake.move('right');
		else if(key_pressed[Key.up_arrow] && snake.direction !== 'down')
			snake.move('up');
		else if(key_pressed[Key.down_arrow] && snake.direction !== 'up')
			snake.move('down');
		else
			snake.move(snake.direction);
	}

	function draw() {
		drawScore();
		snake.draw();
		setFood();
	}

	function clearCell(x, y) {
		context.clearRect(y*Cell.size - Game.offSet , x*Cell.size - Game.offSet, Cell.size + 2*Game.offSet, Cell.size + 2*Game.offSet);
	}

	function changeCellColor(x, y, color) {
		context.fillStyle = color;
		if(x >= 0 && x < Grid.rows)
			if(y >= 0 && y < Grid.columns)
				context.fillRect(y*Cell.size, x*Cell.size, Cell.size, Cell.size);
	}

	function getCanvasContext() {
		canvas = document.createElement("canvas");
		canvas.setAttribute("id", "cnvs");
		canvas.setAttribute("style", "border: 3px solid black;");
		canvas.width = Grid.columns * Cell.size;
		canvas.height = Grid.rows * Cell.size;
		document.body.appendChild(canvas);
		return canvas.getContext("2d");
	}

	main();

})();
